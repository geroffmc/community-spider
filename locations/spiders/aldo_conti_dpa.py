
import scrapy
import pycountry
import json
from locations.items import GeojsonPointItem
from locations.categories import Code


class AldoSpider(scrapy.Spider):
    name = "aldo_conti_dpa"
    brand_name = 'Aldo Conti'
    spider_type = 'chain'
    spider_chain_id = "20930"
    spider_categories = [Code.CLOTHING_AND_ACCESSORIES.value]
    spider_countries = [pycountry.countries.lookup('MEX').alpha_3]
    allowed_domains = ["https://aldoconti.com"]

    # start_urls = ["https://aldoconti.com"]

    def start_requests(self):
        base_url = "https://stockist.co/api/v1/u15743/locations/all"
        yield scrapy.Request(url=base_url, callback=self.parse)

    def parse(self, response):
        datos_json = json.loads(response.body)

        for row in datos_json:
            datos = {
                'ref': row['id'],
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'addr_full': row['address_line_1'],
                'city': row ['city'],
                'state': row ['state'],
                'country': row ['country'],
                'postcode': row ['postal_code'],
                'email':row["email"],
                'website': "https://aldoconti.com",
                'lat': row['latitude'],
                'lon': row['longitude'],
                
            }
            yield GeojsonPointItem(**datos)