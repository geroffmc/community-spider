import scrapy
import pycountry
import uuid
import re
from bs4 import BeautifulSoup
from locations.items import GeojsonPointItem
from locations.categories import Code


class Dominos7elevenorwSpider(scrapy.Spider):
    name = 'seven_eleven_nor_dpa'
    brand_name = '7-Eleven'
    spider_type = 'chain'
    spider_chain_id = '425'
    spider_categories = [Code.CONVENIENCE_STORE.value]
    spider_countries = [pycountry.countries.lookup('nor').alpha_3]
    allowed_domains = ['https://7-eleven.no/butikker']

    # start_urls = ['https://7-eleven.no/butikker']

    def start_requests(self):
        url = 'https://7-eleven.no/butikker'

        yield scrapy.Request(
            url,
            method="GET",
            callback=self.parse
        )

    def parse(self, response):
        soup = BeautifulSoup(response.text, features="lxml")
        
        list_items = soup.find_all('li')

        latitud = 'data-lat'
        longitud= 'data-lng'

        index=1
        stores_list = []
        
        for li in list_items:
            store = {}
            add = li.find('div', class_='street-address')
            cities = li.find('span', class_='locality')
            
            if latitud in li.attrs:
                # Obtener el valor del atributo deseado
                latitude = li.attrs[latitud]
                store['lat'] = latitude
                longitude = li.attrs[longitud] 
                store['lon'] = longitude   
                
            if add!=None:
                # Obtener el contenido del <div>
                address = add.get_text()
                store['addr_full'] = address
                city = cities.get_text()
                store['city'] = city
        
                stores_list.append(store)
        
        for item in stores_list:
            store_final = {
                'ref': uuid.uuid4().hex,
                'addr_full': item['addr_full'],
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'website': 'https://7-eleven.no',
                'city': item['city'],
                'lat': float(item['lat']),
                'lon': float(item['lon']),
            }
            
            yield GeojsonPointItem(**store_final)
    
        
        
        