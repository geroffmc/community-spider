import scrapy
from locations.categories import Code
import pycountry
import re
import uuid
from locations.items import GeojsonPointItem
from bs4 import BeautifulSoup
import json

class MedexpressSpider(scrapy.Spider):
    name = 'medexpress_us_dpa'
    brand_name = 'MedExpress'
    spider_chain_id = "34646"
    spider_type = 'chain'
    spider_categories = [Code.MEDICAL_SERVICES_CLINICS.value]
    spider_countries = [pycountry.countries.lookup('us').alpha_3]
    allowed_domains = ['medexpress.com']

    start_urls = ['https://www.medexpress.com/bin/optum3/medexserviceCallToYEXT2']

    def parse_hours(self, hours_string):
        try:
            hours_mapping = {
                "1": "Su",
                "2": "Mo",
                "3": "Tu",
                "4": "We",
                "5": "Th",
                "6": "Fr",
                "7": "Sa",
            }
            
            hours_list = hours_string.split(",")

            for hours in hours_list:
                day = hours_mapping[hours[0]]
                hours = hours[2:]
                hours = "-".join(re.findall(r'\d+:\d+', hours))
                
                yield f"{day} {hours}"
        except:
            yield ""


    def parse(self, response):
        response_data = response.json()
        places = response_data.get("locations", [])

        for place in places:            
            unparsed_opening_hours = place.get("hours", "")
            opening_hours = "; ".join(list(self.parse_hours(unparsed_opening_hours)))

            if "closed" not in place.get("locationName", "").lower():
                feature = {
                    'ref': place.get("uid", ""),
                    'chain_id': "34646",
                    'chain_name': "MedExpress",
                    'city': place.get("city", ""),
                    'state': place.get("state", ""),
                    'street': place.get("address", ""),
                    'postcode': place.get("zip", ""),
                    'country': place.get("countryCode", ""),
                    'store_url': place.get("displayWebsiteUrl", ""),
                    'opening_hours': opening_hours,
                    'website': 'https://www.medexpress.com',
                    'phone': [place.get("phone", "")],
                    'lat': float(place.get("yextDisplayLat", "")),
                    'lon': float(place.get("yextDisplayLng", "")),
                }

                yield GeojsonPointItem(**feature)
