import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List, Dict
from datetime import datetime, timedelta
from locations.items import GeojsonPointItem


class SparAUT(scrapy.Spider):
    name = "spar_aut_dpa"
    brand_name = 'Spar'
    spider_type= 'chain'
    spider_chain_id = "1087"
    spider_categories = [Code.CONVENIENCE_STORE.value]
    spider_countries = [pycountry.countries.lookup('aut').alpha_3]
    min_lat, max_lat = 46.4000001, 49.0200001
    min_lng, max_lng = 9.5300001, 17.1500001
    step = 0.02

    # start_urls = ["https://www.spar.at"]

    def is_inside_country_polygon(self, lat, lng):
        country_hexagon_vertices = [
            (47.477111, 9.741050),
            (48.898833, 15.399086),
            (47.941289, 17.037273),
            (46.815607, 15.853681),
            (46.473183, 14.481088),
            (47.093233, 9.678392),
        ]
        n = len(country_hexagon_vertices)
        inside = False
        p1x, p1y = country_hexagon_vertices[0]
        for i in range(n + 1):
            p2x, p2y = country_hexagon_vertices[i % n]
            if lng > min(p1y, p2y):
                if lng <= max(p1y, p2y):
                    if lat <= max(p1x, p2x):
                        if p1y != p2y:
                            xinters = (lng - p1y) * (p2x - p1x) / (p2y - p1y) + p1x
                        if p1x == p2x or lat <= xinters:
                            inside = not inside
            p1x, p1y = p2x, p2y
        return inside


    def start_requests(self):
        base_url = "https://www.spar.at/standorte/_jcr_content.stores.v2.html?latitude="
        for lat in range(int(self.min_lat / self.step), int(self.max_lat / self.step)):
            for lng in range(int(self.min_lng / self.step), int(self.max_lng / self.step)):
                #if (self.is_inside_country_polygon(lat * self.step, lng * self.step)):
                url = base_url + f"{lat * self.step}&longitude={lng * self.step}&radius=40&hour=&filter="
                yield scrapy.Request(url=url, callback=self.parse)

    def extract_opening_hours(self, shop_hours):
        days_mapping = {
            'Montag': 'Mo',
            'Dienstag': 'Tu',
            'Mittwoch': 'We',
            'Donnerstag': 'Th',
            'Freitag': 'Fr',
            'Samstag': 'Sa',
            'Sonntag': 'Su'
        }

        formatted_hours = []
        for hours in shop_hours:
            day_type = hours['openingHours']['dayType']
            day_type_short = days_mapping.get(day_type, day_type)

            if all(key in hours['openingHours'] for key in ['from1', 'to1', 'from2', 'to2']):
                from1 = hours['openingHours']['from1']
                to1 = hours['openingHours']['to1']
                from2 = hours['openingHours']['from2']
                to2 = hours['openingHours']['to2']

                if from1 and to1:
                    formatted_hours.append(
                        f"{day_type_short}: {from1['hourOfDay']}:{from1['minute']:02d}-{to1['hourOfDay']}:{to1['minute']:02d}")
                if from2 and to2:
                    formatted_hours.append(
                        f"{day_type_short}: {from2['hourOfDay']}:{from2['minute']:02d}-{to2['hourOfDay']}:{to2['minute']:02d}")
            else:
                formatted_hours.append(f"{day_type_short}: off")

        return '; '.join(formatted_hours)

    def parse(self, response):
        json_data = response.json()
        if not json_data:
            return
        for row in json_data:
            data = {
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'ref': row['locationId'],
                'store_url': 'https://www.spar.at/' + row['pageUrl'],
                'addr_full': row['city'] + ', ' + row['address'],
                'city': row['city'],
                'lat': row['latitude'],
                'lon': row['longitude'],
                'website': 'https://www.spar.at/standorte',
                'phone': row['telephone'],
                "opening_hours": self.extract_opening_hours(row['shopHours']),
            }
        yield GeojsonPointItem(**data)
