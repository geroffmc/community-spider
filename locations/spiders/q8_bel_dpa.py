import scrapy
import pycountry
import json
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List, Dict
from datetime import datetime, timedelta
from locations.items import GeojsonPointItem


class Q8_bel(scrapy.Spider):
    name = "q8_bel_dpa"
    brand_name = 'Q8'
    spider_type = 'chain'
    spider_chain_id = "82"
    spider_categories = [Code.PETROL_GASOLINE_STATION.value]
    spider_countries = [
        pycountry.countries.lookup('be').alpha_3,
        pycountry.countries.lookup('nld').alpha_3,
        pycountry.countries.lookup('lux').alpha_3
    ]

    start_urls = ["https://www.q8.be/fr/get/stations.json"]

    def format_schedule(self, opening_hours):
        weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
        schedule = ''
        for day in weekdays:
            day_schedule = opening_hours.get(day)
            if not day_schedule:
                day_schedule = 'off'
            schedule += f"{day[:2]}: {day_schedule}; "
        return schedule

    def parse(self, response):
        json_data = response.json()['Stations']['Station']
        for row in json_data:
            #postcode, city = row['Address_line_2'].split(" ")
            data = {
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'ref': row['StationId'],
                'country': row['Country'],
                'addr_full': row['Address_line_1'] + ', ' + row['Address_line_2'],
                'phone': row['Phone'],
                'email': row['Email'],
                #'city': city,
                #'postcode': postcode,
                'lat': row['XCoordinate'],
                'lon': row['YCoordinate'],
                'website': 'https://www.q8.be/fr/stations',
                'store_url': 'https://www.q8.be' + row['NodeURL'],
                'opening_hours': self.format_schedule(row['OpeningHours']),
            }
            yield GeojsonPointItem(**data)