# -*- coding: utf-8 -*-
import scrapy
import re
from locations.items import GeojsonPointItem
import uuid
from locations.categories import Code
import pycountry

class KombetareSpider(scrapy.Spider):
    
    name = "kombetare_dpa"
    brand_name = "Banka Kombetare Tregtare"
    spider_type = "chain"
    spider_chain_id = "301777"
    spider_categories = [Code.BANK, Code.ATM]
    spider_countries = [pycountry.countries.lookup('al').alpha_3]
    allowed_domains = ["www.bkt.com.al"]

    start_urls = ["https://www.bkt.com.al/bkt-backoffice/Exchanges/GetAtmList"]

    def parse_phone(self, phone):
        return ''.join(re.findall('\d*', phone))[0:11]

    def parse(self, response):
        '''
        @url https://www.bkt.com.al/bkt-backoffice/Exchanges/GetAtmList
        @returns items 590 610
        @scrapes ref name addr_full postcode city phone email website opening_hours lat lon
        '''

        data = response.json()

        for index, row in enumerate(data['Data']):
            item = GeojsonPointItem()

            item['ref'] = uuid.uuid4().hex
            item['addr_full'] = row['unitAddress'].replace('\n', ',').replace("\"", "")
            item['city'] = row.get('uniCity')
            item['postcode'] = row.get('unitCode')
            item['phone'] = [self.parse_phone(row['phone'])]
            item['email'] = [row['mailInfo']]
            item['website'] = 'https://www.bkt.com.al/'
            item['opening_hours'] = [row['workingHoursEn']]
            item['lat'] = row['unitCordNDec']
            item['lon'] = row['unitCordEDec']

            yield item