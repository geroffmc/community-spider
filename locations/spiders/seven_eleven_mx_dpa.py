import scrapy
import pycountry
import uuid
from locations.items import GeojsonPointItem
from locations.categories import Code

class SevenElevenMxSpider(scrapy.Spider):
    name = 'seven_eleven_mx_dpa'
    brand_name = '7-Eleven'
    spider_type = 'chain'
    spider_chain_id = '425'
    spider_categories = [Code.CONVENIENCE_STORE.value]
    spider_countries = [pycountry.countries.lookup('mx').alpha_3]
    allowed_domains = ['7-eleven.com.mx', 'app.7-eleven.com.mx']
    
    # start_urls = ['https://7-eleven.com.mx']

    def start_requests(self):
        yield scrapy.Request(
            url='https://app.7-eleven.com.mx:8443/web/services/tiendas?key=xc3d',
            method='GET',
            callback=self.parse
        )

    def parse(self, response):
        '''
        @url https://app.7-eleven.com.mx:8443/web/services/tiendas?key=xc3d
        @returns items 2000 2200
        @scrapes addr_full lat lon
        '''
        responseData = response.json()

        for item in responseData['results']:
            opening_hours = ''
            if item.get('open_hours') == "24 horas":
                opening_hours = '24/7'
            else:
                opening_hours = item.get('open_hours', '')
            
            store = {
                'ref': uuid.uuid4().hex,
                'chain_name': "7-Eleven",
                'chain_id': "425",
                'addr_full': item.get('full_address'),
                'website': 'https://www.7-eleven.com.mx/',
                'opening_hours': opening_hours,
                'lat': item.get('latitude'),
                'lon': item.get('longitude'),
            }
            
            yield GeojsonPointItem(**store)
