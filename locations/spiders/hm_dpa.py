import scrapy
import pycountry
import locations
from locations.items import GeojsonPointItem
from locations.categories import Code
import json
import uuid
from typing import List, Dict


class HMSpider(scrapy.Spider):
    name = 'hm_dpa'
    brand_name = 'H&M'
    spider_type = 'chain'
    spider_chain_id = '4199'
    spider_categories = [Code.CLOTHING_AND_ACCESSORIES.value]
    spider_countries = ["ALL"]
    
    # start_urls = ["https://www2.hm.com"]

    def start_requests(self):
        yield scrapy.Request(
            url="https://api.storelocator.hmgroup.tech/v2/brand/hm/locations/locale/nl_nl/countries",
            method='GET',
            callback=self.parse
        )

    def parse(self, response):
        response_countries = response.json()

        for item in response_countries['storeCountries']:
            country_id = item['countryId']
            
            yield scrapy.Request(
                url=f"https://api.storelocator.hmgroup.tech/v2/brand/hm/stores/locale/de_at/country/{country_id}?_type=json&campaigns=true&departments=true&openinghours=true",
                method='GET',
                callback=self.parse_places
            )
            
    def parse_places(self, response):
        response_data = response.json()

        for location in response_data['stores']:
            data = {
                'ref': uuid.uuid4().hex,
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'street': location.get('address', {}).get('streetName1'),
                'city': location.get('city'),
                'country': location.get('country', ''),
                'postcode': location.get('address', {}).get('postCode'),
                'state' : location.get('address', {}).get('state', ''),
                'opening_hours': self.parse_hours(location.get('openingHours', [])),
                'phone': location.get('phone', ''),
                'website': 'https://www2.hm.com/',
                'lat': location.get('latitude', ''),
                'lon': location.get('longitude', ''),
            }

            yield GeojsonPointItem(**data)

    def parse_hours(self, hours):
        opening_hours = []

        day_map = {
            1: 'Mo',
            2: 'Tu',
            3: 'We',
            4: 'Th',
            5: 'Fr',
            6: 'Sa',
            7: 'Su',
        }

        for item in hours:
            day = day_map[item['day']]
            time_open = item['opens']
            time_close = item['closes']

            opening_hours.append(f"{day} {time_open}-{time_close}")
        
        return ";".join(opening_hours)
