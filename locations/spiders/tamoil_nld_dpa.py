# -*- coding: utf-8 -*-
from lxml import etree
import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List, Dict
import uuid

import re

class TAMOIL_NLD(scrapy.Spider):
    name = 'tamoil_nld_dpa'
    brand_name = 'TAMOIL'
    spider_chain_id = "81"
    spider_type = 'chain'
    spider_categories = [Code.PETROL_GASOLINE_STATION.value]
    spider_countries = [pycountry.countries.lookup('nld').alpha_3]

    start_urls = ["https://tamoil.nl/tankstations"]

    def parse(self, response):
        tree = response
        twodrots = f'//*[@id="locationList"]/div'
        treex = tree.xpath(twodrots)

        for i in range(1,len(treex)+1):

            title_path = f'//*[@id="locationList"]/div[{i}]/div/p/text()'
            title = tree.xpath(title_path)[0].get()

            address_path = f'//*[@id="locationList"]/div[{i}]/div/div[1]/p/text()'
            address = tree.xpath(address_path)

            street_and_number = address[0].get().strip()
            address_full = street_and_number

            postcode_and_city = address[1].get().strip()
            postcode, city = postcode_and_city.rsplit(' ', 1)

            address_full += f", {postcode} {city}"


            phone_path = f'//*[@id="locationList"]/div[{i}]/div/div[2]/p/text()'
            phone = tree.xpath(phone_path)
            if (len(phone)>1):
               phone=phone[1].get().strip()
            else:
                phone=''
            data = {
                'ref': uuid.uuid4().hex,
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'addr_full': address_full,
                'city': city,
                'postcode': postcode,
                'phone': phone,
                'website': 'https://tamoil.nl/tankstations',
            }
            yield GeojsonPointItem(**data)
