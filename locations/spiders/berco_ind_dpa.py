import scrapy
import locations
import json
import uuid
import pycountry
from locations.categories import Code
from locations.items import GeojsonPointItem


class BercoINDSpider(scrapy.Spider):
    name = 'berco_ind_dpa'
    brand_name = "Berco's"
    spider_type = 'chain'
    spider_chain_id = '34280'
    spider_categories = [Code.RESTAURANT.value]
    spider_countries = [pycountry.countries.lookup('IN').alpha_3]
    allowed_domains = ["https://bercos.net.in"]
    
    # start_urls = ["https://bercos.net.in/"]

    def start_requests(self):
        url = "https://assets.limetray.com/assets/image_manager/uploads/507/bercos-location-v3.json"
        
        yield scrapy.Request(
            url=url,
            method='GET',
            callback=self.parse
        )

    def parse(self, response):
        response_data = response.json()
        for location in response_data['location']:
            coordinates = location['prop']['coordinates'].split(',')
            lat = coordinates[0].strip()
            lon = coordinates[1].strip()

            data = {
                'ref': uuid.uuid4().hex,
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'addr_full': location['prop']['address'],
                'city': location['city'],
                'phone': location['prop']['phone'],
                'email': location['prop']['mail'],
                'opening_hours': location['prop']['timings'],
                'website': 'https://bercos.net.in/',
                'lat': lat,
                'lon': lon,
            }
            yield GeojsonPointItem(**data)
