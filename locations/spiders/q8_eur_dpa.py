import scrapy
import pycountry
import json
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List, Dict
from datetime import datetime, timedelta
from locations.items import GeojsonPointItem


class Q8_EUR(scrapy.Spider):
    name = "q8_eur_dpa"
    brand_name = 'Q8'
    spider_type = 'chain'
    spider_chain_id = "82"
    spider_categories = [Code.PETROL_GASOLINE_STATION.value]
    spider_countries = [
        pycountry.countries.lookup('ALB').alpha_3,
        pycountry.countries.lookup('AND').alpha_3,
        pycountry.countries.lookup('AUT').alpha_3,
        pycountry.countries.lookup('BLR').alpha_3,
        pycountry.countries.lookup('BEL').alpha_3,
        pycountry.countries.lookup('BIH').alpha_3,
        pycountry.countries.lookup('BGR').alpha_3,
        pycountry.countries.lookup('HRV').alpha_3,
        pycountry.countries.lookup('CYP').alpha_3,
        pycountry.countries.lookup('CZE').alpha_3,
        pycountry.countries.lookup('DNK').alpha_3,
        pycountry.countries.lookup('EST').alpha_3,
        pycountry.countries.lookup('FIN').alpha_3,
        pycountry.countries.lookup('FRA').alpha_3,
        pycountry.countries.lookup('DEU').alpha_3,
        pycountry.countries.lookup('GRC').alpha_3,
        pycountry.countries.lookup('HUN').alpha_3,
        pycountry.countries.lookup('ISL').alpha_3,
        pycountry.countries.lookup('IRL').alpha_3,
        pycountry.countries.lookup('ITA').alpha_3,
        pycountry.countries.lookup('KAZ').alpha_3,
        pycountry.countries.lookup('LVA').alpha_3,
        pycountry.countries.lookup('LIE').alpha_3,
        pycountry.countries.lookup('LTU').alpha_3,
        pycountry.countries.lookup('LUX').alpha_3,
        pycountry.countries.lookup('MKD').alpha_3,
        pycountry.countries.lookup('MLT').alpha_3,
        pycountry.countries.lookup('MDA').alpha_3,
        pycountry.countries.lookup('MCO').alpha_3,
        pycountry.countries.lookup('MNE').alpha_3,
        pycountry.countries.lookup('NLD').alpha_3,
        pycountry.countries.lookup('NOR').alpha_3,
        pycountry.countries.lookup('POL').alpha_3,
        pycountry.countries.lookup('PRT').alpha_3,
        pycountry.countries.lookup('ROU').alpha_3,
        pycountry.countries.lookup('RUS').alpha_3,
        pycountry.countries.lookup('SMR').alpha_3,
        pycountry.countries.lookup('SRB').alpha_3,
        pycountry.countries.lookup('SVK').alpha_3,
        pycountry.countries.lookup('SVN').alpha_3,
        pycountry.countries.lookup('ESP').alpha_3,
        pycountry.countries.lookup('SWE').alpha_3,
        pycountry.countries.lookup('CHE').alpha_3,
        pycountry.countries.lookup('UKR').alpha_3,
        pycountry.countries.lookup('GBR').alpha_3,
        pycountry.countries.lookup('VAT').alpha_3
    ]

    start_urls = ["https://ids.q8.com/en/get/stations.json"]

    def parse(self, response):
        json_data = response.json()['Stations']['Station']
        for row in json_data:
            data = {
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'ref': row['StationId'],
                'name': row['Name'],
                'country': row['Country'],
                'addr_full': row['Address_line_1'] + ', ' + row['Address_line_2'],
                'lat': row['XCoordinate'],
                'lon': row['YCoordinate'],
                'website': 'https://ids.q8.com/en/stations',
                'store_url': 'https://ids.q8.com' + row['NodeURL'],
                'opening_hours': "24/7",
            }
            yield GeojsonPointItem(**data)