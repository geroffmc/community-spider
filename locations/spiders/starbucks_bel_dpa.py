import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List, Dict
from datetime import datetime, timedelta
from locations.items import GeojsonPointItem


class StarbucksBEL(scrapy.Spider):
    name = "starbucks_bel_dpa"
    brand_name = 'Starbucks'
    spider_type = 'chain'
    spider_chain_id = "1396"
    spider_categories = [Code.COFFEE_SHOP.value]
    spider_countries = [pycountry.countries.lookup('bel').alpha_3]
    min_lat, max_lat = 49.5000001, 51.5000001
    min_lng, max_lng = 2.5000001, 6.5000001
    step = 0.1

    # start_urls = ["https://www.starbucks.be"]

    def start_requests(self):
        base_url = "https://www.starbucks.be/nl/api/v1/store-finder?latLng="
        for lat in range(int(self.min_lat / self.step), int(self.max_lat / self.step)):
            for lng in range(int(self.min_lng / self.step), int(self.max_lng / self.step)):
                if (self.is_inside_norway_polygon(lat * self.step, lng * self.step)):
                    url = base_url + f"{lat * self.step}%2C{lng * self.step}"
                    yield scrapy.Request(url=url, callback=self.parse)

    def is_inside_norway_polygon(self, lat, lng):
        bel_hexagon_vertices = [
            (51.115132, 2.647930),
            (50.542826, 3.391238),
            (50.155625, 4.295700),
            (49.734499, 5.688068),
            (50.863978, 5.610106),
            (51.411119, 5.035347),
            (51.356599, 3.348123),
        ]

        n = len(bel_hexagon_vertices)
        inside = False
        p1x, p1y = bel_hexagon_vertices[0]
        for i in range(n + 1):
            p2x, p2y = bel_hexagon_vertices[i % n]
            if lng > min(p1y, p2y):
                if lng <= max(p1y, p2y):
                    if lat <= max(p1x, p2x):
                        if p1y != p2y:
                            xinters = (lng - p1y) * (p2x - p1x) / (p2y - p1y) + p1x
                        if p1x == p2x or lat <= xinters:
                            inside = not inside
            p1x, p1y = p2x, p2y
        return inside

    def parse(self, response):
        json_data = response.json()['stores']
        for row in json_data:
            if row['description'] != 'Closed':
                data = {
                    'chain_name': self.brand_name,
                    'chain_id': self.spider_chain_id,
                    'ref': row['id'],
                    'addr_full': row['address'],
                    'phone': row['phoneNumber'],
                    'lat': float(row['coordinates']['lat']),
                    'lon': float(row['coordinates']['lng']),
                    'website': 'https://www.starbucks.be',
                    "opening_hours": '',
                }
                days = {
                    'Monday': 'Mo',
                    'Tuesday': 'Tu',
                    'Wednesday': 'We',
                    'Thursday': 'Th',
                    'Friday': 'Fr',
                    'Saturday': 'Sa',
                    'Sunday': 'Su',
                }

                opening_hours = ''

                for hours in row['hoursNext7Days']:
                    name = hours['name']
                    if name == 'Today':
                        name = self.get_day_abbr()
                    elif name == 'Tomorrow':
                        name = self.get_day_abbr(0)
                    else:
                        name = days.get(name, '')
                    if (name and hours['description'] != 'Closed'):
                        start_time, end_time = hours['description'].split(' to ')
                        start_time = datetime.strptime(start_time, '%I:%M %p').strftime('%H:%M')
                        end_time = datetime.strptime(end_time, '%I:%M %p').strftime('%H:%M')
                        opening_hours += f'{name} {start_time}-{end_time}; '

                data['opening_hours'] = opening_hours.strip()
                yield GeojsonPointItem(**data)

    def get_day_abbr(self, days=-1):
        days += datetime.today().weekday()
        return ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'][days]