# -*- coding: utf-8 -*-

import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code

class SupergasSpider(scrapy.Spider):
    name = "supergas_dpa"
    brand_name = "Super Gas"
    spider_chain_id = "28203"
    spider_type = "chain"
    spider_categories = [Code.PETROL_GASOLINE_STATION.value]
    spider_countries = [pycountry.countries.lookup('ind').alpha_3]
    allowed_domains = ["supergas.com"]

    # start_urls = ["https://www.supergas.com/api/dealer/searchbyarea"]
    
    def start_requests(self):
        url =  'https://www.supergas.com/api/dealer/searchbyarea'
        payloads = [
            '{"northEast":{"lat":37.00648595239184,"lng":98.25784705031866},"southWest":{"lat":1.7404973904385437,"lng":62.09085486281866},"zoomLevel":5,"dealerType":"Cylinder","sortBy":"Name"}',
            '{"northEast":{"lat":37.00648595239184,"lng":98.25784705031866},"southWest":{"lat":1.7404973904385437,"lng":62.09085486281866},"zoomLevel":5,"dealerType":"Autogas","sortBy":"Name"}'
        ]
        headers = {
            'Content-Type': 'application/json'
        }

        for payload in payloads:
            yield scrapy.Request(
                method='post',
                url=url,
                body=payload,
                headers=headers
            )


    def parse(self, response):
        '''
        @url https://www.supergas.com/
        @returns 373
        @scrapes ref website brand email name phone lat lon
        '''

        responseData = response.json()['Dealers']

        for row in responseData:
            try:
                phone: list = row['dealerPhones'][0].split(",")
            except:
                phone: list = []
            
            data = {
                'ref': row['id'],
                'chain_id': "28203",
                'chain_name': "Super Gas",
                'email': [row['email']],
                'name': row['description'],
                'phone': phone,
                'website': 'https://www.supergas.com/',
                'lat': float(row['geometry']['location']['lat']),
                'lon': float(row['geometry']['location']['lng']),
            }
            
            yield GeojsonPointItem(**data)