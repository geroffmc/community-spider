# -*- coding: utf-8 -*-
import json
import re
import uuid

import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List, Dict

class VoltasSpider(scrapy.Spider):
    name = 'voltas_dpa'
    brand_name = 'Voltas'
    spider_type = 'chain'
    spider_chain_id = "34420"
    spider_categories = [Code.CONSUMER_ELECTRONICS_STORE.value]
    spider_countries = [pycountry.countries.lookup('in').alpha_3]
    allowed_domains = ['www.myvoltas.com']

    # start_urls = ['https://www.myvoltas.com/storelocator/dealerData.php']

    def start_requests(self):

        url = 'https://www.myvoltas.com/storelocator/dealerData.php'

        i = 0
        while i < 33:
            i += 1
            yield scrapy.FormRequest(
                url=url,
                method= "POST",
                formdata={"state": str(i)},
                callback=self.second_requests
            )

    def second_requests(self, response):

        surl = "https://www.myvoltas.com/storelocator/getstore.php"

        values = response.xpath('//option/@value').getall()

        for elem in values:

            yield scrapy.FormRequest(
                url=surl,
                method= "POST",
                formdata={"city": str(elem)},
                callback=self.parse
            ) 

    def parse(self, response):
        item = GeojsonPointItem()
        data = response

        if data != None:

            stores = data.json()
            for store in stores:
                item['ref'] = uuid.uuid1().hex
                item['addr_full'] = store['StoreAddress1']
                if store['StorePhone'] != "":
                    item['phone'] = re.split(",|/", store['StorePhone'])
                item['city'] = store['CityName']
                item['website'] = "https://www.myvoltas.com"
                item['email'] = "vcare@voltas.com"
                if store['lat'] != "":
                    item['lat'] = store['lat']
                    item['lon'] = store['longt']

                item['chain_name'] = self.brand_name
                item['chain_id'] = self.spider_chain_id

                yield item
