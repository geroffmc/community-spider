import scrapy
from locations.items import GeojsonPointItem
from locations.categories import Code
import pycountry

class BricomarcheSpider(scrapy.Spider):
    name = "bricomarche_dpa"
    brand_name = "Bricomarché"
    spider_type = "chain"
    spider_chain_id = "3380"
    spider_categories = [Code.HOME_IMPROVEMENT.value]
    spider_countries = [pycountry.countries.lookup('pl').alpha_3]
    allowed_domains = ["www.bricomarche.pl"]

    start_urls = ["https://www.bricomarche.pl/api/v1/pos/pos/poses.json"]
    
    def parse(self, response):
        '''
        @url https://www.bricomarche.pl/api/v1/pos/pos/poses.json
        @returns items 590 610
        @scrapes ref name addr_full street housenumber postcode country city state phone website email lat lon
        '''
        data = response.json()

        for row in data['results']:
            item = GeojsonPointItem()

            street = row.get('Street')
            city = row.get('City')
            country = 'Polska'
            housenumber = row.get('HouseNumber')
            postcode = row.get('Postcode')
            state = row.get('Province')
            phone = [row.get('Phone')]
            email = [row.get('Email')]

            item['ref'] = row['Id']
            item['name'] = row['Name']
            item['addr_full'] = f"{housenumber}, {street}, {city}, {state}, {country}, {postcode}"
            item['street'] = street
            item['housenumber'] = housenumber
            item['city'] = city
            item['state'] = state
            item['postcode'] = postcode
            item['country'] = country
            item['phone'] = phone
            item['website'] = 'https://www.bricomarche.pl/'
            item['email'] = email
            item['lat'] = float(row.get('Lat')) if row.get('Lat') != "" else None
            item['lon'] = float(row.get('Lng')) if row.get('Lng') != "" else None

            yield item