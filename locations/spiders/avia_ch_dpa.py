import scrapy
from locations.items import GeojsonPointItem
from locations.categories import Code
import pycountry
import uuid
import re

class AviaCHSpider(scrapy.Spider):
    name = 'avia_ch_dpa'
    brand_name = 'AVIA'
    spider_type = 'chain'
    spider_chain_id = '16'
    spider_categories = [Code.PETROL_GASOLINE_STATION.value]
    spider_countries = [pycountry.countries.lookup('CHE').alpha_3]
    
    # start_urls = ['https://www.avia.ch/tankstellenfinder/jsonpassthru.php']

    def start_requests(self):
        url = 'https://www.avia.ch/tankstellenfinder/jsonpassthru.php'
        headers = {
            'authority': 'www.avia.ch',
            'accept': '*/*',
            'accept-language': 'es-ES,es;q=0.9',
            'referer': 'https://www.avia.ch/tankstellenfinder/de',
            'sec-ch-ua': '^\\^Not.A/Brand^\\^;v=^\\^8^\\^, ^\\^Chromium^\\^;v=^\\^114^\\^, ^\\^Google',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '^\\^Windows^\\^',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-origin',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36',
            'x-requested-with': 'XMLHttpRequest',
        }

        yield scrapy.Request(
            url=url, 
            headers=headers, 
            callback=self.parse
            )


    def parse(self, response):
        response_stations = response.json()

        for station in response_stations:
            
            if station['Montag.VonBis']:
                opening_hours = ";".join([
                    f"Mon {station['Montag.VonBis']}",
                    f"Tue {station['Dienstag.VonBis']}",
                    f"Wed {station['Mittwoch.VonBis']}",
                    f"Thu {station['Donnerstag.VonBis']}",
                    f"Fri {station['Donnerstag.VonBis']}",
                    f"Sat {station['Samstag.VonBis']}",
                    f"Sun {station['Sonntag.VonBis']}",
                ])
            else:
                opening_hours = ""

            data = {
                'ref': uuid.uuid4().hex,
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'street': station['Strasse'],
                'housenumber': station['StrasseNummer'],
                'city': station['Ort'],
                'country': station['Country'],
                'phone': station['Telefon'],
                'opening_hours': opening_hours,
                'state': station['Kanton'],
                'postcode': station['PLZ'],
                'website': 'https://avia.ch/',
                'lon': station['GEO.X'],
                'lat': station['GEO.Y'],
            }

            yield GeojsonPointItem(**data) 
