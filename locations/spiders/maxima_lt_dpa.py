# -*- coding: utf-8 -*-
import re
import scrapy
from locations.items import GeojsonPointItem
from locations.categories import Code
import pycountry

class MaximaSpider(scrapy.Spider):
    
    name = 'maxima_lt_dpa'
    brand_name = "Maxima"
    spider_type = "chain"
    spider_chain_id = "2820"
    spider_categories = [Code.GROCERY.value]
    spider_countries = [pycountry.countries.lookup('ltu').alpha_3]
    allowed_domains = ["maxima.lt"]

    # start_urls = ["https://www.maxima.lt/ajax/shopsnetwork/map/getCities"]

    def start_requests(self):
        yield scrapy.FormRequest(
            url = "https://www.maxima.lt/ajax/shopsnetwork/map/getCities",
            method = 'POST',
            callback = self.parse,
            formdata = {"mapId": "1"}
        )

    def parse_hours(self, opening_hours):
        days = {
            'VII': 'Su',
            'VI': 'Sa',
            'V': 'Fr',
            'IV': 'Th',
            'III': 'We',
            'II': 'Tu',
            'I': 'Mo',
            '.': ':'
        }

        for key, value  in days.items():
            opening_hours = opening_hours.replace(key, value)
        
        return opening_hours
    
    def parse(self, response):
        '''
        @url https://www.maxima.lt/ajax/shopsnetwork/map/getCities
        @scrapes ref addr_full website lat lon
        '''
        data = response.json()

        for row in data:
            item = GeojsonPointItem()

            item['ref'] = row.get('id')
            item['addr_full'] = row.get('address')
            item['opening_hours'] = self.parse_hours(row.get('time'))
            item['website'] = "https://maxima.lt"
            item['lat'] = float(row.get('lat'))
            item['lon'] = float(row.get('lng'))

            yield item