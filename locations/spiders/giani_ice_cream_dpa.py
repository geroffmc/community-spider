import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List, Dict
from bs4 import BeautifulSoup

import uuid
from locations.items import GeojsonPointItem


class GianiIC(scrapy.Spider):
    name = "giani_ice_cream_dpa"
    brand_name = 'Giani Ice Cream'
    spider_type = 'chain'
    spider_chain_id = "301791"
    spider_categories = [Code.RESTAURANT.value]
    spider_countries = [pycountry.countries.lookup('ind').alpha_3]
    
    # start_urls = ["http://www.gianiicecream.in"]

    def start_requests(self):
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36'}
        url = 'http://www.gianiicecream.in/our_outlets.php'

        yield scrapy.Request(url=url, headers=headers, callback=self.parse)

    def parse(self, response):
        soup = BeautifulSoup(response.text, 'html.parser')
        ice = soup.findAll('span', class_='franchise_Label')
        for item in ice:
            phone=item.next_sibling.next_sibling.next_sibling.next_sibling
            
            if (phone.find('/')):
                phone=phone.split('/')
            
            data = {
                'ref': uuid.uuid4().hex,
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'addr_full': item.next_sibling.next_sibling,
                'phone': phone,
                'website': 'http://www.gianiicecream.in/our_outlets.php',
            }
            
            yield GeojsonPointItem(**data)
