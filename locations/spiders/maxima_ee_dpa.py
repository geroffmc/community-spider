# -*- coding: utf-8 -*-
import re
import scrapy
from locations.items import GeojsonPointItem
from locations.categories import Code
import pycountry

class MaximaSpider(scrapy.Spider):
    
    name = "maxima_ee_dpa"
    brand_name = "Maxima"
    spider_type = "chain"
    spider_chain_id = "2820"
    spider_categories = [Code.GROCERY.value]
    spider_countries = [pycountry.countries.lookup('est').alpha_3]
    allowed_domains = ["maxima.ee"]

    # start_urls = ["https://www.maxima.ee/ajax/shopsnetwork/map/getCities"]

    def start_requests(self):
        yield scrapy.FormRequest(
            url = "https://www.maxima.ee/ajax/shopsnetwork/map/getCities",
            method = 'POST',
            callback = self.parse,
            formdata = {"mapId": "2"}
        )
    
    def parse_hours(self, opening_hours):
        days = {
            'P': 'Su',
            'L': 'Sa',
            'R': 'Fr',
            'N': 'Th',
            'K': 'We',
            'T': 'Tu',
            'E': 'Mo',
            '.': ':'
        }

        for key, value  in days.items():
            opening_hours = opening_hours.replace(key, value)
        
        return opening_hours

    def parse(self, response):
        '''
        @url https://www.maxima.ee/ajax/shopsnetwork/map/getCities
        @scrapes ref addr_full website lat lon
        '''
        features = response.json()

        for row in features:
            data = {
                'ref': row.get('id'),
                'chain_id': "2820",
                'chain_name': "Maxima",
                'addr_full': row.get('address'),
                'opening_hours': self.parse_hours(row.get('time')),
                "website": "https://maxima.ee",
                "lat": float(row.get('lat')),
                "lon": float(row.get('lng')),
            }

            yield GeojsonPointItem(**data)