import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List, Dict


class EtisalatSpider(scrapy.Spider):
    name = 'etisalat_uae_dpa'
    brand_name = 'Etisalat'
    spider_type = 'chain'
    spider_chain_id = ''
    spider_categories = [Code.TELEPHONE_SERVICE.value]
    spider_countries = [pycountry.countries.lookup('ae').alpha_3]
    allowed_domains = ['etisalat.ae']

    # start_urls = ["https://www.etisalat.ae/content/dam/etisalat/prod-mock-assets/storesnew.json"]

    def start_requests(self):
        url = "https://www.etisalat.ae/content/dam/etisalat/prod-mock-assets/storesnew.json"
        headers = {
            'sdata': 'eyJjaGFubmVsIjoid2ViIiwiYXBwbGljYXRpb25fb3JpZ2luIjoiaW53aS5tYSIsInV1aWQiOiIwMmU1NmNhOS03ZTBjLTQ5YzktYmVjZS1hNGRmZWI5ODEzOWYiLCJsYW5ndWFnZSI6ImZyIiwiYXBwVmVyc2lvbiI6MX0='
        }

        yield scrapy.Request(
            url=url,
            headers=headers,
            callback=self.parse
        )

    # Need to add opening hours
    # def parse_hours(self, hours):
    #     pass

    def parse(self, response):
        responseData = response.json()

        for row in responseData['data']:
            data = {
                'ref': row['locationId'],
                'addr_full': row['address1'],
                'city': row['address2'],
                'country': row['country'],
                'phone': row['phoneNumber'],
                # 'opening_hours': row['information'],
                'website': 'https://www.etisalat.ae/',
                'lat': float(row['lat']),
                'lon': float(row['lng']),
            }

            yield GeojsonPointItem(**data)
