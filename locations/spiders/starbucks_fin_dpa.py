import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List
from datetime import datetime

class StarbucksFIN(scrapy.Spider):
    name = "starbucks_fin_dpa"
    brand_name = 'Starbucks'
    spider_type = 'chain'
    spider_chain_id = "1396"
    spider_categories = [Code.COFFEE_SHOP.value]
    spider_countries = [pycountry.countries.lookup('fin').alpha_3]
    min_lat, max_lat = 59.808303, 70.096933
    min_lng, max_lng = 20.561517, 31.587366
    step = 1

    # start_urls = ["https://www.starbucks.com"]

    def start_requests(self):
        base_url = "https://www.starbucks.com/bff/locations?lat="
        headers={
            "accept": "application/json",
            "accept-language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
            "newrelic": "eyJ2IjpbMCwxXSwiZCI6eyJ0eSI6IkJyb3dzZXIiLCJhYyI6IjEzMDc1MTkiLCJhcCI6IjI0NTQ5MzA1IiwiaWQiOiJjMDZhZThhNzBjNWI1ZGU2IiwidHIiOiJjNTdlY2JiNzYxZGRjMmMyZGE5Mjc0ZjVmY2IwNmNjMCIsInRpIjoxNjgyOTY2NTk4MTU1LCJ0ayI6IjEzMDYzMTIifX0=",
            "sec-ch-ua": "\"Chromium\";v=\"112\", \"Google Chrome\";v=\"112\", \"Not:A-Brand\";v=\"99\"",
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": "\"Windows\"",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "traceparent": "00-c57ecbb761ddc2c2da9274f5fcb06cc0-c06ae8a70c5b5de6-01",
            "tracestate": "1306312@nr=0-1-1307519-24549305-c06ae8a70c5b5de6----1682966598155",
            "x-dq7hy5l1-a": "T924Tf=7NaGZrlY6-oU1ZHFQ8nehYQFrXyjPoc6sU3UFgngk9yWBx9R380nGc3OG7WDoiwKLcDy5=9lzDNr-gp3bGgNZ5t1_0T5EbLdz2cBLxeEYPHtFzXGccAuCho1H7W7TF2Y1LNstdHCOpe=h4fiop4Xx6UWGbVLONgkEy_l1gQXwo1cjur33iFRVgrXqFdZpHHTiwLCzOZh8cQ=A0ng=vZLJKPHapMgzZUwJB8o=HuoZvaTfooKzUbn8l-0LqlxLEilcQkDnLN8jYZCWhQ0s57NHs4kbalKzWxZGbkGPdO0YDNNiRK9D81ZKzxadw98kQUlwi_tc9JskdPDTqPbpTG2x4O4Ov7cCdUoK_5q90t7dHW=oRwbCZ132np92nbCOClbjZN3jQrpxKdTP24f2OseoGpagB4kcyBxqgt0WfVwTrBq2AUC8PhCY4_x0fY_1K1sIriGw5MBtG=ZJWqHn_UH8n8tEf3brxDOIVf__dAI=XKX-XrvesRDAw-JtTY2yVs20HZhq=-8lfx6gjJAg4aPsdcH5GQIXgWI3RGMfatpYbMdDaj_bP-mRxUhzEVRbc9slCyIJi8A=pa7tEtJ5MG0FsFfEuaR49Gryka32-8v4Cfw7CfywPGzb-8sI9GMIkQxWIMu-4aNURp_q7wAdvtuXt5UolZWI=rWKMffgjCFP8dHkNnjnT_C8gBCfXYJTbMrnfkvuCsUWeG80OUl8rmWQ-_fJgouR2Q9u9357yd4mahKVuClZHN905wJe2dwcjVCmj1kpJE4JpuCl5GqRimATqAcWqwXPcBw6YHI12TpOMy_9jQmLQF9QGrNVc269smEhF0ddjTeHKwFdH=txhebaaq89Yj_luJcvCBO13kGep8VC6zlh4B-PkrAL7-neJG9NenJ4NI=F6ffC7ZgzxpErn-RIkr2eUQ9t6WlLOGcI0n2f0QYyUVdpgwCfNXmeb7boYLiBIn1bWLMABdloj5emd4k7-Qovv1hoK35=aWw3DwLdks9PX5mdE7H9zgLZaEzEThFerdvGiVumMh3hTCwsLbHV4qAEcWRGc_Cj1yNLuXiqk267Q4z62lXZCX2svKa1uyPm6t3Dyu-azqC=QOFxQ6hhJgQzARF=5oy2M99hQFcwtkn17K2tHdt8t18Og=NY_EWCTmnmVsMGHR9iC4NdvgjBTvbgdTMPTmJ0YtWac=ICIyc1Ao5gzz6VMaQ0RdbKTpZuj06rFZbq0kHi0E4aoiwHbJP=gCyINMNOiQsfZLgf9IV68zRt4FLKtcBBGhPmwsevT1ikDNE=guCnNLgJur4Gj6m_JM34QAuqPaYFbBYUHWfmwwgyMbR9ThWrx7ylLBu7ZX=0nCX1L84YrbgDFKk4guW2RyD1Fp4Q--9liDqbX-NK=H1E7HzqVCTVyx-ARhmK79CvQ2qdsMecMPmz1qgGOYV1dvAbijwNuxjj9eRoc6QjDY4KywgeJeCz_o-hQ83efhYE_Z-LaImPCCJl1Et8fmORDeortJp4IoBEkkB3RsAmZbYJoFkgFe-tGM3eVIXwTm_4n_3OpcJiW63Bm-QgtCJN1d=V68pzHcUnsJgARC5h2Hlxx8NJDUAsefli-9BV-60Ki2soGEEWTz-k9OR9Mk_F86lqJgABdYHPx6JH1a0tb_mLPiViEArWuw=MYdhFItsqwLPDnK7gEzz73z7vfxfBVc9W4LnCHKIHxBArdFi2CJ3cjJMddKWEhIDwbvJU_7qdFAgeoXlK2uNnPqh-06xOsYFt0TBV9CT8AIR4axtAHeX3=oATbLDn1QsqWEdbxkOh_R017zcpK6gf=XHxF_e6iGH=izOw5-oJ-hDGgKt0CwzJIMRl1rlRq_RMQ9q9zdIsvPnAX47FGLkZUXzrZY0MZPf-iqz63cyE2=nUrKmuv7Y33_p6QNi9LIUsjWH3M_PPJcxuwbpk98Kf8ub8PtBEsWQvKtCcX9NUJLC_IpUnRFYzOTjz8JFT0raj9HgqVIELdtvkAN92CyiWTiF5pvBCfuRg5uRfLJIJ70ypB=lNQZN_uXqHIqMoTezhF-ua9Kd99KW3010scHyDP2VzAok6YuorG0dN1VVGtLDmsW43ojQifxad=mAWFDsE4qZiLWtr4KyeoBLmVbh1bJEkjc=DA-mjGw0KtpQTgFFVAsD4klBYJmtpdmvJQesPTdvk4sbFjlwE_caWZiCHYts2mJnv6soTYIZxHaqt=b-3LEQIM3kKqUZU_-tY9BiDs=KqDBm_qglEjFbO4L_ikFpU8hKvtFIep1oYwBzUGcYMl5D9trDd-ttu1JYjtONoTEV2BGuRYWxRQ=RIbUXt4hG28tfzX-phBny-xUQ4kPLdnbWbqUUWimyfKDAs=QsRhlL87ZsBM3GP6sKL=KMKcKY=Ufbxg=to7zGRXcA1TRwEs45_nTFUGMw72RBGV=5P1ONnegbTPaAW91bE8DrFckKc_mQDaXKjbct2nNczEs4qG6wl4_RVfQ-PLMrY1bwOCp7AkPgz1M21vB05xNXwGyLvoMOmBrTt8vyt4nsFPOU-Y89tpRamJNKnLPtqbrn_xLjuYF57T1beNv47uw=lXJ0Iy_hYPy8w=U2EpcaLvWj7PgtYKAz0Dt3naRnmg-R_fjoJCAciWiH9NjBvQA85_5jGJupphKMGtdUcH8X16ZrMZ9B-sBE-L1XiQqs_1rxwnl3iAIFt5JtQ8OU3kFvTNsIIw83cwQD28OQnp1liv1A_9eqn_XDWmEm1GD_rastOI1J28Y_6eCWIBWT4pG6_jIXfOLqR6m3GiLWKok-owHD9zevvEwunAu4HjaVAphNBnaF3yIaQkXzJGVywe9p3F5uW4GzRaB2igiVAr4xGgAxMaq50NfaDF_ynZVD6xPIjtRqqb4_bOMQOGUJZO31QDnQxFjt8Jc-KFq53s1JdiP60y97Nsig02AtkDJPW=oiDoXG8ewjsDi=lD1Zw3Ux-3K_qKbzrEhieTI3kymDImpRJ=Rlg1DIuAHe0ehFkCt4FX8EJ3BbaE=BEGB-u2F0aVwFvp2hl2X7Ktg7UAM=bqrwiLQDrYK3-WxOq0MU0tq3xUL6TjY5UyTJ6EBopBakCmz3xpPtD1ZjBLnZnOmRcc4-Pf8r5K1b5fNUAs6b4TF-yM9eAQY34xbKUP6kKktaQJgVvnsl1uUe3ZEqwl9slcy",
            "x-dq7hy5l1-b": "-kpn8hj",
            "x-dq7hy5l1-c": "AEAklNiHAQAAHh3BTqhal59DuKyImOJnZEk90mUwaD-SP2W32sJrn6J_xaM3",
            "x-dq7hy5l1-d": "ABaAhIjBCKHFgQGAAYIQgISi0aIA5JmBzvpjzz8Awmufon_FozcAAAAAAdPYgAE0RVjAIGh8Y7hoWyEoyuyT",
            "x-dq7hy5l1-f": "A0kwn9iHAQAAaUF8MB_pTJ-R82uPM3aW6_f9FJqLT6fNbz052wRkh0V0d6jkAQU7lYauchZ2wH8AAEB3AAAAAA==",
            "x-dq7hy5l1-z": "q",
            "x-requested-with": "XMLHttpRequest",
            "cookie": "ux_exp_id=d5eee769-3224-4daf-9e06-7f066bdacb48; tiWQK2tY=AzjGVceHAQAAOXMmsTWIR65_CXY3MvkchpgVIZmHX4CTODfcdcn8sfN95HCIAQU7lYauchZ2wH8AAEB3AAAAAA|1|0|f7dec73b58c6b327927d57a5727a47ea99d0d496; _gcl_au=1.1.1843346501.1682676571; fp_token_7c6a6574-f011-4c9a-abdd-9894a102ccef=94pXxevHmVifIkQuZ8p+v6FlEvX74u2SP4ufnyK9wfk=; AKA_A2=A; TAsessionID=f0f3931c-b33e-484f-9b50-b3275c5161b7|EXISTING; _gid=GA1.2.1525878491.1682966014; _uetsid=ade983b0e84e11ed8487e74034f2e4a6; _uetvid=c4ad21a0e5ac11ed85314bddfec33c55; _ga=GA1.1.1512943826.1682676571; _ga_VMTHZW7WSM=GS1.1.1682966013.4.1.1682966596.0.0.0; notice_behavior=implied,us; RT=\"z=1&dm=www.starbucks.com&si=5d942e9c-386f-49b8-8114-41c438e58bec&ss=lh56id4x&sl=1&tt=37x&rl=1\"; RHARrQj1; notice_gdpr_prefs=0,1,2:; notice_preferences=2:; cmapi_gtm_bl=; cmapi_cookie_privacy=permit 1,2,3",
            "Referer": "https://www.starbucks.com/store-locator/store/1013670/helsinki-kamppi-urho-kekkosen-katu-1-helsinki-18-00100-fi",
            "Referrer-Policy": "strict-origin-when-cross-origin"
        }
        for lat in range(int(self.min_lat / self.step), int(self.max_lat / self.step)):
            for lng in range(int(self.min_lng / self.step), int(self.max_lng / self.step)):
                url = base_url + f"{lat * self.step}&lng={lng * self.step}&mop=true"
                yield scrapy.Request(url=url, headers=headers, callback=self.parse)

    def parse(self, response):
        json_data = response.json()['stores']
        for row in json_data:
                data = {
                    'ref': row['id'],
                    'chain_name': self.brand_name,
                    'chain_id': self.spider_chain_id,
                    'street': row['address']['streetAddressLine1'],
                    'city': row['address']['city'],
                    'postcode': row['address']['postalCode'],
                    'country': row['address']['countryCode'],
                    'lat': float(row['coordinates']['latitude']),
                    'lon': float(row['coordinates']['longitude']),
                    'website': 'https://www.starbucks.com/',
                    'store_url': 'https://www.starbucks.com/store-locator/store/'+row['id']+'/'+row['slug'],
                    "opening_hours": '',
                }
                days = {
                    'Monday': 'Mo',
                    'Tuesday': 'Tu',
                    'Wednesday': 'We',
                    'Thursday': 'Th',
                    'Friday': 'Fr',
                    'Saturday': 'Sa',
                    'Sunday': 'Su',
                }

                opening_hours = ''

                for hours in row['schedule']:
                    name = hours['dayName']
                    if name == 'Today':
                        name = self.get_day_abbr()
                    elif name == 'Tomorrow':
                        name = self.get_day_abbr(1)
                    else:
                        name = days.get(name, '')
                    if (name):
                        start_time, end_time = hours['hours'].split(' to ')
                        start_time = datetime.strptime(start_time, '%I:%M %p').strftime('%H:%M')
                        end_time = datetime.strptime(end_time, '%I:%M %p').strftime('%H:%M')
                        opening_hours += f'{name} {start_time}-{end_time}; '

                data['opening_hours'] = opening_hours.strip()
                yield GeojsonPointItem(**data)

    def get_day_abbr(self, days=0):
        days += datetime.today().weekday()
        return ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'][days]
