# -*- coding: utf-8 -*-
from lxml import etree
import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code
import uuid

import re

class GULF_UAE(scrapy.Spider):
    
    name = 'gulf_uae_dpa'
    brand_name = 'Gulf'
    spider_chain_id = "83"
    spider_type = 'chain'
    spider_categories = [Code.PETROL_GASOLINE_STATION.value]
    spider_countries = [pycountry.countries.lookup('are').alpha_3]

    start_urls = ["https://me.gulfoilltd.com/en/dealer-locator"]

    def parse(self, response):
        tree = response
        twodrots = f'//*[@id="block-progressive-sub-content"]/section/div'
        treex = tree.xpath(twodrots)
        count = 11
        count_path = f'//*[@id="block-progressive-sub-content"]/section/div/div/div/div[2]/div[2]/div/div[1]/h4/text()'
        match = re.search(r'\((\d+)\)', tree.xpath(count_path)[0].get())
        
        if match:
            result = int(match.group(1))
            count = result
        for i in range(1,count+1):

            title_path = f'//*[@id="block-progressive-sub-content"]/section/div/div/div/div[2]/div[2]/div/div[1]/ul/li[{i}]/h5/text()'
            title = tree.xpath(title_path)[0].get().strip()

            address_path = f'//*[@id="block-progressive-sub-content"]/section/div/div/div/div[2]/div[2]/div/div[1]/ul/li[{i}]/address/text()[2]'
            address = tree.xpath(address_path)[0].get().strip()

            city_path = f'//*[@id="block-progressive-sub-content"]/section/div/div/div/div[2]/div[2]/div/div[1]/ul/li[{i}]/address/text()[3]'
            city = tree.xpath(city_path)[0].get().strip()

            country_path = f'//*[@id="block-progressive-sub-content"]/section/div/div/div/div[2]/div[2]/div/div[1]/ul/li[{i}]/address/text()'
            country = tree.xpath(country_path)[3].get().strip()

            phone_path = f'//*[@id="block-progressive-sub-content"]/section/div/div/div/div[2]/div[2]/div/div[1]/ul/li[{i}]/address/div/a/text()'
            phone = tree.xpath(phone_path).get()

            data = {
                'ref': uuid.uuid4().hex,
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'addr_full': city + ', ' + address,
                'city': city,
                'country': country,
                'phone': phone,
                'website': 'https://me.gulfoilltd.com/en/dealer-locator',
            }

            yield GeojsonPointItem(**data)
