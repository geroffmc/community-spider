# -*- coding: utf-8 -*-
from lxml import etree
import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List, Dict
import uuid

import re

class KFC_LVA(scrapy.Spider):
    name = 'kfc_lva_dpa'
    brand_name = 'KFC'
    spider_chain_id = "1559"
    spider_type = 'chain'
    spider_categories = [Code.FAST_FOOD.value]
    spider_countries = [pycountry.countries.lookup('lva').alpha_3]

    def convert_schedule(self,schedule):
            schedule = schedule.replace("Darba laiks:", "")

            # Удаление ненужных символов
            # schedule = re.sub(r"[^A-Za-z0-9:-]", "", schedule)

            # Замена дней недели на соответствующие сокращения
            schedule = schedule.replace("S.", "Sa")
            schedule = schedule.replace("Pk", "Fr")
            schedule = schedule.replace("Sv", ";Su")
            schedule = schedule.replace("P", "Mo")
            schedule = schedule.replace("T", "We")
            schedule = schedule.replace("O", "Tu")

            schedule = schedule.replace("C", "Th")
            schedule = schedule.replace('.', '')

            # Добавление разделителей между днями недели и временными интервалами
            schedule = re.sub(r"([A-Za-z]{2})(\d{2}:\d{2})", r"\1 \2", schedule)

            return schedule

    start_urls = ["https://kfc.lv/restorani/"]

    def parse(self, response):

        tree = response

        for i in range(2, 7):
            # Получение названия
            title_path = f'/html/body/div/div[2]/div/div/main/article/div/div/div/div/section[{i}]/div/div/div[1]/div/div/div[2]/div/h2/text()'
            title = tree.xpath(title_path).get()

            # Получение адреса
            address_path = f'/html/body/div/div[2]/div/div/main/article/div/div/div/div/section[{i}]/div/div/div[1]/div/div/div[3]/div/h2/text()'
            address = tree.xpath(address_path).get()

            # Получение телефона
            phone_path = f'/html/body/div/div[2]/div/div/main/article/div/div/div/div/section[{i}]/div/div/div[2]/div/div/section/div/div/div[2]/div/div/div/div/h2/a/text()'
            phone = tree.xpath(phone_path).get()


            # Получение расписания
            schedule_path = f'/html/body/div[1]/div[2]/div/div/main/article/div/div/div/div/section[{i}]/div/div/div[3]/div/div//h2/text()'
            schedule = tree.xpath(schedule_path).getall()
            opening_hours=""
            if len(schedule) > 0:
                for j in range(0, len(schedule)):
                    opening_hours=opening_hours+schedule[j]
            data = {
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'ref': uuid.uuid4().hex,
                'addr_full': address,
                'website': 'https://kfc.lv',
                'phone': phone,
                'opening_hours': self.convert_schedule(opening_hours),
            }
            yield GeojsonPointItem(**data)
