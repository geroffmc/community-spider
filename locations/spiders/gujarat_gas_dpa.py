# -*- coding: utf-8 -*-

import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List, Dict
import json
import uuid


class GujaratGasSpider(scrapy.Spider):
    name = "gujarat_gas_dpa"
    brand_name = "Gujarat Gas"
    spider_chain_id = "28204"
    spider_type = "chain"
    spider_categories = [Code.PETROL_GASOLINE_STATION]
    spider_countries = [pycountry.countries.lookup('ind').alpha_3]
    allowed_domains = ["gujaratgas.com"]

    # start_urls = ["https://iconnect.gujaratgas.com/Portal/FootPrintPrelogin.aspx/SearchFP"]

    def start_requests(self):
        url = 'https://iconnect.gujaratgas.com/Portal/FootPrintPrelogin.aspx/SearchFP'

        coords = [[23.708363, 68.643403], [23.208363, 68.643403], [23.708363, 69.143403], [23.208363, 69.143403], [22.208363, 69.143403], [23.208363, 69.643403], [22.208363, 69.643403], [21.708363, 69.643403], [23.208363, 70.143403], [22.208363, 70.143403], [21.708363, 70.143403], [23.708363, 70.643403], [23.208363, 70.643403], [22.708363, 70.643403], [22.208363, 70.643403], [21.708363, 70.643403], [23.208363, 71.143403], [22.708363, 71.143403], [22.208363, 71.143403], [21.708363, 71.143403], [21.208363, 71.143403], [23.208363, 71.643403], [22.708363, 71.643403], [22.208363, 71.643403], [21.708363, 71.643403], [21.208363, 71.643403], [24.208363, 72.143403], [23.708363, 72.143403], [23.208363, 72.143403], [22.708363, 72.143403], [22.208363, 72.143403], [21.708363, 72.143403], [24.208363, 72.643403], [
            23.708363, 72.643403], [23.208363, 72.643403], [22.708363, 72.643403], [21.708363, 72.643403], [23.708363, 73.143403], [23.208363, 73.143403], [22.708363, 73.143403], [22.208363, 73.143403], [21.708363, 73.143403], [21.152358, 73.25786], [20.708363, 73.143403], [20.208363, 73.143403], [23.208363, 73.643403], [22.708363, 73.643403], [22.208363, 73.643403], [21.708363, 73.643403], [21.208363, 73.643403], [22.708363, 74.143403], [22.295136, 70.799346], [22.565949, 72.897509], [21.177558, 72.785761], [20.500294, 72.98894], [24.935873, 72.462777], [23.174885, 74.417959], [19.863687, 72.827767], [21.361886, 73.147381], [21.853326, 73.295956], [22.471846, 72.604171], [28.314, 72.0510], [31.639, 75.429], [18.498, 74.708], [11.209, 77.519], [28.654, 77.306], [20.974, 78.799], [27.398, 70.409]]
        # A few coords for test
        # coords = [[23.708363,68.643403], [23.208363,68.643403], [23.708363,69.143403], [23.208363,69.143403]]

        for c in coords:
            lat, lon = str(c[0]), str(c[1])

            payload = {"searchString": "", "LocationTypeId": "0",
                       "Latitude": lat, "Longitude": lon}
            payload = json.dumps(payload)

            headers = {
                'Content-Type': 'application/json; charset=UTF-8',
                'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'
            }

            yield scrapy.Request(
                method='post',
                url=url,
                body=payload,
                headers=headers
            )

    def parse(self, response):
        '''
        @url https://iconnect.gujaratgas.com/Portal/FootPrintPrelogin.aspx/SearchFP
        @returns 697
        @scrapes ref brand name extras phone lat lon
        '''

        responseData = json.loads(response.json()['d'])

        for row in responseData:
            data = {
                'ref': uuid.uuid4().hex,
                'chain_id': "28204",
                'chain_name': "Gujarat Gas",
                'phone': [row['PhoneNo']],
                'website': 'https://gujaratgas.com',
                'lat': float(row['Latitude']),
                'lon': float(row['Longitude']),
            }

            yield GeojsonPointItem(**data)
