from scrapy import spiderloader
from scrapy.utils import project
from metadata_tests import MetadataTests


class TestShellCHESpider(MetadataTests):

    @classmethod
    def setup_class(self):
        settings = project.get_project_settings()
        spider_loader = spiderloader.SpiderLoader.from_settings(settings)
        self.spider_instance = spider_loader.load("shell_che_dpa")
