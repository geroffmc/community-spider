from urllib.parse import urlparse


class MetadataTests:

    # Spider Type
    def test_spider_type(self):
        assert hasattr(self.spider_instance, "spider_type")

    def test_spider_type_values(self):
        if hasattr(self.spider_instance, "spider_type"):
            assert self.spider_instance.spider_type == "chain" or self.spider_instance.spider_type == "generic"

    # Brand Name
    def test_spider_brand_name(self):
        if hasattr(self.spider_instance, "spider_type"):
            if self.spider_instance.spider_type == "chain":
                assert hasattr(self.spider_instance, "brand_name")

    # Chain ID
    def test_spider_chain_id(self):
        assert hasattr(self.spider_instance, "spider_chain_id")

    def test_spider_chain_id_value(self):
        if hasattr(self.spider_instance, "spider_chain_id"):
            assert type(self.spider_instance.spider_chain_id) == str

    # Categories
    def test_spider_categories(self):
        assert hasattr(self.spider_instance, "spider_categories")

    def test_spider_categories_values(self):
        if hasattr(self.spider_instance, "spider_categories"):
            assert all(
                [type(category) == str for category in self.spider_instance.spider_categories])

    # Countries
    def test_spider_countries(self):
        assert hasattr(self.spider_instance, "spider_countries")

    def test_spider_countries_values(self):
        if hasattr(self.spider_instance, "spider_countries"):
            assert all(
                [type(country) == str for country in self.spider_instance.spider_countries])

    # Allowed domains
    def test_allowed_domains_values(self):
        if hasattr(self.spider_instance, "allowed_domains"):
            assert all(
                [type(domain) == str for domain in self.spider_instance.allowed_domains])

    # Start urls
    def test_starts_urls_values(self):
        if hasattr(self.spider_instance, "start_urls"):
            checked_urls: list[bool] = []

            for url in self.spider_instance.start_urls:
                if urlparse(url).scheme == 'https' or urlparse(url).scheme == 'http':
                    checked_urls.append(True)
                else:
                    checked_urls.append(False)

            assert all(checked_urls)
