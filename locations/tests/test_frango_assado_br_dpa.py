from scrapy import spiderloader
from scrapy.utils import project
from metadata_tests import MetadataTests


class TestFrangoSpider(MetadataTests):

    @classmethod
    def setup_class(self):
        settings = project.get_project_settings()
        spider_loader = spiderloader.SpiderLoader.from_settings(settings)
        self.spider_instance = spider_loader.load("frango_assado_br_dpa")
